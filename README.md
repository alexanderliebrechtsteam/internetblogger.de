## Internetblogger.de Repository

In diesem Projekt mit dem Internetblogger.de teile ich mein CMS-Wissen mit dir lieber Projektleser. Bei Bitbucket bin ich seit einer Weile und mir gefällt es hier soweit, sodass ich das Projekt hier haben möchte. OpenSource/Flat File CMS sind ein sehr spannender Bereich, wo immer etwas los ist. Es wird weiterhin an den CMS weltweit weiterentwickelt, seien es WP, Joomla, Drupal, PHP Fusion 9 oder solche Exoten in Deutschland wie das Subrion CMS.

## Wiki in diesem Repo

Hier gibt es den Wiki und die Tickets, die ich erstellen werde. Mir wird bestimmt etwas einfallen, was gepostet und worüber berichtet werden kann. So wird sich das Projekt füllen und ich hoffe, dass es Bitbucket für immer geben wird. Letztendlich gehört es Atlassian aus Australien und von denen habe ich Confluence Server, Bitbucket Server und JIRA Software Server am Laufen. Damit arbeite ich und hier in der Cloud sind meine wenigen Repos.

## Tickets hier im Projekt

Tickets sind auch sehr nützlich, denn diese, wenn sie relevant genug sind, werden über Google und Co. auch wirklich gefunden. Das weiss ich aus meinen Projekt wie dem Phabricator-Projekt. Auch kleinere Foren indexiert Google gut und die Inhalte werden gefunden. Mal sehen, wie intensiv ich das Repo hier betreuen kann.

> Dir wünsche hier fröhliches Umsehen und viel Spass beim Erkunden des Internetblogger.de-Projektes!!

## Kontakt + Impressum

    Alexander Liebrecht
    Alte Warnemünder Ch. 25
    D-18109 Rostock
    Tel: +49 038133795190
    Mail: power_sash@gmx.net